## light-user 12 SP1A.210812.016 V13.0.2.0.SLSINXM release-keys
- Manufacturer: xiaomi
- Platform: mt6833
- Codename: light
- Brand: Redmi
- Flavor: light-user
- Release Version: 12
- Id: SP1A.210812.016
- Incremental: V13.0.2.0.SLSINXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-GB
- Screen Density: 440
- Fingerprint: Redmi/light/light:12/SP1A.210812.016/V13.0.2.0.SLSINXM:user/release-keys
- OTA version: 
- Branch: light-user-12-SP1A.210812.016-V13.0.2.0.SLSINXM-release-keys
- Repo: redmi_light_dump_412


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
